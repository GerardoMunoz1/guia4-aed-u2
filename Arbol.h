#include <iostream>
#include "Nodo.h"

using namespace std;

#ifndef ARBOL_H
#define ARBOL_H

// Clase de punteros a raiz de árbol y métodos para modificarlo
class Arbol{

    private:
        // Puntero al inicio del árbol
        Nodo *raiz = NULL;

    public:
        // Constructor por defecto
        Arbol();
        // Método para añadir elementos al árbol
        void add_elemento(Nodo *nodo, int numero);
        // Método para eliminar elementos del árbol
        void remover_elemento(Nodo *nodo, int numero);
        // Método para cambiar el valor de un nodo
        void modificar_elemento(int numero);
        // Método para recorrer el árbol de distintas formas
        void recorrer_arbol(int modo);
        // Método que retorna la raiz del árbol
        Nodo *get_raiz();
};
#endif
