#include <iostream>
#include <stdlib.h>
#include "Arbol.h"

using namespace std;

// Constructor default
Arbol::Arbol(){}

// Función que añade elementos al árbol
void Arbol::add_elemento(Nodo *nodo, int numero){

    if (this->raiz == NULL){
        Nodo *temp = new Nodo();
        temp->numero = numero;
        this->raiz = temp;
        cout << "Elemento añadido con éxito!";
    }

    else{
        if(numero < nodo->numero){

            if(nodo->izquierda == NULL){
                Nodo *temp = new Nodo();
                temp->numero = numero;
                nodo->izquierda = temp;
                cout << "Elemento añadido con éxito!";
            }
            else{
                add_elemento(nodo->izquierda, numero);
            }
        }
        else if (numero > nodo->numero){

            if(nodo->derecha == NULL){
                Nodo *temp = new Nodo();
                temp->numero = numero;
                nodo->derecha = temp;
                cout << "Elemento añadido con éxito!";
            }
            else{
                add_elemento(nodo->derecha, numero);
            }
        }
        else{
            cout << "El número ingresado ya se encuentra en el árbol" << endl;
        }
    }
}
// Función que retorna la raiz del árbol
Nodo *Arbol::get_raiz(){
    return this->raiz;
}
