#include <iostream>
#include <stdlib.h>
#include "Arbol.h"
#include "Grafo.h"

using namespace std;

void menu(){

    Arbol arbol;
    Grafo generador;
    string opcion;
    string aux;
    string numero;

    while(1){

        cout << "\n\t---Arbol Binario---" << endl << endl;

        cout << "[1] Añadir elemento al árbol" << endl;
        cout << "[2] Remover elemento del árbol" << endl;
        cout << "[3] Modificar un elemento del árbol" << endl;
        cout << "[4] Recorrer el árbol" << endl;
        cout << "[5] Generar grafo correspondiente del árbol" << endl;
        cout << "[6] Salir" << endl;
        cout << "OPCION: ";
        getline(cin, opcion);

        if (opcion == "1"){
            cout << "Ingrese número que desea añadir al árbol: ";
            getline(cin, numero);
            arbol.add_elemento(arbol.get_raiz(), stoi(numero));
        }

        else if (opcion == "5"){
            generador.generar_grafo(arbol.get_raiz());
        }

        else if (opcion == "6"){

            cout << "\nHasta pronto!" << endl;
            break;
        }

        else{
            cout << "Opción no válida, inténtelo nuevamente." << endl;
            system("sleep 1");
        }

        system("clear");
    }
}

int main(){
    
    menu();

    return 0;
}
