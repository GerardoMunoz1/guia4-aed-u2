#include <iostream>
#include <fstream>
#include "Nodo.h"

using namespace std;

#ifndef GRAFO_H
#define GRAFO_H

// Clase grafos
class Grafo{

    public:
        // Constructor por defecto
        Grafo();
        // Función para iniciar escritura y generación de grafo
        void generar_grafo(Nodo *nodo);
        // Función para recorrer elementos del árbol escribiendo archivo temporal
        void recorrer(Nodo *nodo, ofstream &archivo);
};
#endif
